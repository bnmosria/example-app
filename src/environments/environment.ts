export const environment = {
    production: false,
    apiUrls: {
        products: '/api/products',
        categories: '/api/categories',
        product: '/api/product',
        cart: '/api/cart',
        orders: '/api/orders',
    },
};
