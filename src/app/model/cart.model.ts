import { Injectable } from '@angular/core';
import { BaseProduct, Product } from '../products/state/product.model';

@Injectable()
export class Cart {
    public lines: CartLine[] = [];
    public itemCount = 0;
    public cartPrice = 0;

    public addLine(product: BaseProduct, quantity: number = 1) {
        const line = this.lines.find(line => line.product.id === product.id);
        if (line !== undefined) {
            line.quantity += quantity;
        } else {
            this.lines.push(new CartLine(product, quantity));
        }

        console.log(this.lines);

        this.recalculate();
    }

    public updateQuantity(product: BaseProduct, quantity: number) {
        const line = this.lines.find(line => line.product.id === product.id);
        if (line !== undefined) {
            line.quantity = Number(quantity);
        }
        this.recalculate();
    }

    public removeLine(id: number) {
        const index = this.lines.findIndex(line => line.product.id === id);
        this.lines.splice(index, 1);
        this.recalculate();
    }

    public clear() {
        this.lines = [];
        this.itemCount = 0;
        this.cartPrice = 0;
    }

    private recalculate() {
        this.itemCount = 0;
        this.cartPrice = 0;
        this.lines.forEach(l => {
            this.itemCount += l.quantity;
            this.cartPrice += l.quantity * l.product.price;
        });
    }
}

export class CartLine {
    constructor(public product: BaseProduct, public quantity: number) {}

    public get lineTotal() {
        return this.quantity * this.product.price;
    }
}
