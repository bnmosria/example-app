import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseProduct, Product } from '../products/state/product.model';
import { ProductsService } from '../products/state/products.service';
// import { StaticDataSource } from "./static.datasource";
import { RestDataSource } from './rest.datasource';

@Injectable()
export class ProductRepository {
    private products: Product[] = [];
    private categories: string[] = [];

    constructor(
        private dataSource: RestDataSource,
        private productService: ProductsService
    ) {
        /*
        dataSource.getProducts().subscribe(data => {
            this.products = data;
            this.categories = data.map(p => p.category)
                .filter((c, index, array) => array.indexOf(c) == index).sort();
        });*/
    }

    public getCategories(category: string = null): Product[] {
        return [];
    }

    public getProducts(): Observable<BaseProduct[]> {
        return this.productService.getProducts();
    }

    /*
    getProduct(id: number): Product {
        return this.products.find(p => p.id == id);
    }

    getCategories(): string[] {
        return this.categories;
    }
    saveProduct(product: Product) {
        if (product.id == null || product.id == 0) {
            this.dataSource.saveProduct(product)
                .subscribe(p => this.products.push(p));
        } else {
            this.dataSource.updateProduct(product)
                .subscribe(p => {
                    this.products.splice(this.products.
                        findIndex(p => p.id == product.id), 1, product);
                });
        }
    }

    deleteProduct(id: number) {
        this.dataSource.deleteProduct(id).subscribe(p => {
            this.products.splice(this.products.
                findIndex(p => p.id == id), 1);
        });
    }*/
}
