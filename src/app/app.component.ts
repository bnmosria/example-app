import { Component, NgZone } from '@angular/core';
import { akitaDevtools } from '@datorama/akita';
import { environment } from '../environments/environment';

@Component({
    selector: 'app',
    template: '<router-outlet></router-outlet>',
})
export class AppComponent {
    constructor(private ngZone: NgZone) {
        if (environment.production) {
            return;
        }

        akitaDevtools(ngZone, {});
    }
}
