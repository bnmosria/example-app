import { Component } from '@angular/core';

import { ProductRepository } from '../model/product.repository';
import {BaseProduct} from '../products/state/product.model';
import {Observable} from 'rxjs';

@Component({
    templateUrl: 'productTable.component.html'
})
export class ProductTableComponent {

    constructor(private repository: ProductRepository) { }

    getProducts(): Observable<BaseProduct[]> {
        return this.repository.getProducts();
    }

    deleteProduct(id: number) {
        // this.repository.deleteProduct(id);
    }
}
