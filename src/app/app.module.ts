import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { StoreFirstGuard } from './store-first.guard';
import { StoreModule } from './store/store.module';

@NgModule({
    imports: [BrowserModule, StoreModule, AppRoutingModule],
    providers: [StoreFirstGuard],
    declarations: [AppComponent],
    bootstrap: [AppComponent],
})
export class AppModule {}
