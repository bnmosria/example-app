import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

import { CartService } from '../../cart/state/cart.service';
import { BaseProduct } from '../state/product.model';

@Component({
    selector: 'base-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.css'],
})
export class ProductComponent {
    @Input() public product: BaseProduct;
    public quantity: FormControl = new FormControl(1);

    constructor(private cartService: CartService) {}

    public addProductToCart(product: BaseProduct): void {
        console.log(this.quantity.value);

        this.cartService.add(product, this.quantity.value);
    }
}
