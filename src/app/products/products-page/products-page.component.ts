import { Component, OnDestroy, OnInit } from '@angular/core';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { combineLatest, EMPTY, forkJoin, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { BaseProduct } from '../state/product.model';
import { ProductsQuery } from '../state/products.query';
import { ProductsService } from '../state/products.service';

@Component({
    templateUrl: './products-page.component.html',
})
export class ProductsPageComponent implements OnInit, OnDestroy {
    public isLoading$: Observable<boolean>;
    public products$: Observable<BaseProduct[]>;

    constructor(
        private productsService: ProductsService,
        private productsQuery: ProductsQuery
    ) {}

    public ngOnInit(): void {
        this.isLoading$ = this.productsQuery.selectLoading();
        this.products$ = this.productsQuery.selectAll();

        combineLatest([
            this.productsQuery.selectHasCache(),
            this.productsQuery.selectFilters$,
            this.productsQuery.selectSearchTerm$,
        ])
            .pipe(
                switchMap(([cached, filters, term]) => {
                    return cached ? EMPTY : this.productsService.getProducts();
                }),
                untilDestroyed(this)
            )
            .subscribe({
                error() {
                    // show error
                },
            });
    }

    public getData() {
        const snacksAndCategories$ = forkJoin([
            this.productsService.getProducts(),
            this.productsService.getCategories(),
        ]);

        return this.productsQuery.getHasCache() ? EMPTY : snacksAndCategories$;
    }

    public ngOnDestroy(): void {}
}
