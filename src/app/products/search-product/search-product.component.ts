import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ProductsQuery } from '../state/products.query';
import { ProductsService } from '../state/products.service';

@Component({
    selector: 'app-search-product',
    templateUrl: './search-product.component.html',
})
export class SearchProductComponent implements OnInit {
    public searchControl = new FormControl();

    constructor(
        private productsService: ProductsService,
        private productsQuery: ProductsQuery
    ) {}

    public ngOnInit() {
        this.searchControl.patchValue(this.productsQuery.searchTerm);

        this.searchControl.valueChanges
            .pipe(
                debounceTime(300),
                distinctUntilChanged()
            )
            .subscribe(term => this.productsService.updateSearchTerm(term));
    }
}
