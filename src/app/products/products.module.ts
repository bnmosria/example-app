import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ProductComponent } from './product/product.component';

@NgModule({
    declarations: [ProductComponent],
    imports: [CommonModule, ReactiveFormsModule, RouterModule],
    exports: [ProductComponent],
})
export class ProductsModule {}
