import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { tap } from 'rxjs/operators';
import { ProductsQuery } from '../state/products.query';
import { ProductsService } from '../state/products.service';

@Component({
    selector: 'app-filters',
    templateUrl: './filters.component.html',
    styleUrls: ['./filters.component.css'],
})
export class FiltersComponent implements OnInit {
    public filters = new FormGroup({
        condition: new FormControl(),
        location: new FormControl(),
        deliveryOption: new FormControl(false),
    });

    constructor(
        private productsService: ProductsService,
        private productsQuery: ProductsQuery
    ) {}

    public ngOnInit() {
        this.filters.patchValue(this.productsQuery.filters);

        this.filters.valueChanges
            .pipe(
                tap(() => this.productsService.invalidateCache()),
                untilDestroyed(this)
            )
            .subscribe(filters => this.productsService.updateFilters(filters));
    }

    public ngOnDestroy() {}
}
