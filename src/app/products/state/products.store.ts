import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Category } from './category.model';
import { BaseProduct } from './product.model';

export interface ProductsState extends EntityState<BaseProduct> {
    searchTerm: string;
    ui: {
        selectedCategory: Category;
    };
    filters: {
        condition: string;
        location: string;
        deliveryOption: boolean;
    };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'products' })
export class ProductsStore extends EntityStore<ProductsState, BaseProduct> {
    constructor() {
        super({
            searchTerm: '',
            ui: {
                selectedCategory: null,
            },
            filters: {
                condition: null,
                location: null,
                deliveryOption: false,
            },
        });
    }
}
