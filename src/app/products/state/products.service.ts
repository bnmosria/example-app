import { HttpClient } from '@angular/common/http';
import { Injectable, OnDestroy } from '@angular/core';
import { ID } from '@datorama/akita';
import { tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

import { EMPTY, forkJoin, Observable } from 'rxjs';
import { Category } from './category.model';
import { BaseProduct, Product } from './product.model';
import { ProductsQuery } from './products.query';
import { ProductsStore } from './products.store';

@Injectable()
export class ProductsService implements OnDestroy {
    constructor(
        private productsStore: ProductsStore,
        private productsQuery: ProductsQuery,
        private http: HttpClient
    ) {}

    public ngOnDestroy(): void {
        this.productsStore.destroy();
    }

    public getProducts(): Observable<BaseProduct[]> {
        return this.http
            .get<BaseProduct[]>(environment.apiUrls.products)
            .pipe(
                tap((products: BaseProduct[]) =>
                    this.productsStore.set(products)
                )
            );
    }

    public getCategories(): Observable<Category[]> {
        return this.http
            .get<Category[]>(environment.apiUrls.categories)
            .pipe(
                tap((categories: Category[]) =>
                    this.productsStore.update({ categories })
                )
            );
    }

    public updateSelectedCategory(category: Category): void {
        this.productsStore.update({
            ui: {
                selectedCategory: category,
            },
        });
    }

    public getData() {
        const snacksAndCategories$ = forkJoin([
            this.getProducts(),
            this.getCategories(),
        ]);

        return this.productsQuery.getHasCache() ? EMPTY : snacksAndCategories$;
    }

    public getProduct(id: ID) {
        return this.http
            .get<Product>(`${environment.apiUrls.product}/${id}`)
            .pipe(tap(product => this.productsStore.upsert(id, product)));
    }

    public updateFilters(filters) {
        this.productsStore.update({ filters });
    }

    public invalidateCache() {
        this.productsStore.setHasCache(false);
    }

    public updateSearchTerm(searchTerm: string) {
        this.productsStore.update({ searchTerm });
        this.invalidateCache();
    }
}
