import { Injectable } from '@angular/core';
import { filterNil, ID, QueryEntity } from '@datorama/akita';
import { Observable } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';
import { Category } from './category.model';
import { BaseProduct, Product } from './product.model';
import { ProductsState, ProductsStore } from './products.store';

@Injectable({ providedIn: 'root' })
export class ProductsQuery extends QueryEntity<ProductsState, BaseProduct> {
    public selectFilters$ = this.select('filters');
    public selectSearchTerm$ = this.select('searchTerm');

    public selectCategories$ = this.select(state => state.categories);
    public selectedCategory$ = this.select(state => state.ui.selectedCategory);

    constructor(protected store: ProductsStore) {
        super(store);
    }

    public static filterBy(
        category: Category
    ): (product: BaseProduct) => any | boolean {
        return (product: BaseProduct) => {
            if (category) {
                return product.category === category.id;
            }

            return true;
        };
    }

    public selectVisibleProducts(): Observable<BaseProduct[]> {
        return this.selectedCategory$.pipe(
            switchMap((category: Category) => {
                return this.selectAll({
                    filterBy: ProductsQuery.filterBy(category),
                });
            })
        );
    }

    public get filters(): any {
        return this.getValue().filters;
    }

    public get searchTerm(): string {
        return this.getValue().searchTerm;
    }

    public hasProduct(id: ID): boolean {
        return this.hasEntity(id) && !!this.getEntity(id);
    }

    public selectProduct(id: ID): Observable<BaseProduct> {
        return this.selectEntity(id).pipe(
            filterNil,
            filter(({ category }) => !!category)
        );
    }
}
