import { ID } from '@datorama/akita';

export interface AdditionalData {
    productAdjective: string;
    productMaterial: string;
    department: string;
    color: string;
}

export interface BaseProduct {
    id: ID;
    name: string;
    image: string;
    description: string;
    category: ID;
    price: number;
}

export interface Product extends BaseProduct {
    additionalData?: AdditionalData;
}
