import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { CartPageRoutingModule } from './cart-page-routing.module';

import { CartPageComponent } from './cart-page/cart-page.component';

@NgModule({
    declarations: [CartPageComponent],
    imports: [CartPageRoutingModule, CommonModule, ReactiveFormsModule],
})
export class CartModule {}
