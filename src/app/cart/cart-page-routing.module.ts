import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StoreFirstGuard } from '../store-first.guard';
import { CartPageComponent } from './cart-page/cart-page.component';

const routes: Routes = [
    {
        path: '',
        component: CartPageComponent,
        canActivate: [StoreFirstGuard],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CartPageRoutingModule {}
