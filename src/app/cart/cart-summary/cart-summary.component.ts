import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { CartQuery } from '../state/cart.query';

@Component({
    selector: 'cart-summary',
    templateUrl: 'cart-summary.component.html',
})
export class CartSummaryComponent {
    public count$: Observable<number> = this.cartQuery.selectCount();
    public total$: Observable<number> = this.cartQuery.selectTotal$;

    constructor(private cartQuery: CartQuery) {}
}
