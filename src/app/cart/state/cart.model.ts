import { ID } from '@datorama/akita';
import { BaseProduct } from '../../products/state/product.model';

export interface CartItem {
    productId: ID;
    quantity: number;
    product: BaseProduct;
    total: number;
}
