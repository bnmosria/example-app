import { Injectable } from '@angular/core';
import { ID, QueryEntity} from '@datorama/akita';
import { map } from 'rxjs/operators';
import { CartItem } from './cart.model';
import { CartState, CartStore } from './cart.store';

@Injectable({ providedIn: 'root' })
export class CartQuery extends QueryEntity<CartState, CartItem> {
    public selectTotal$ = this.selectAll().pipe(
        map(items => items.reduce((acc, item) => acc + item.total, 0))
    );

    constructor(protected store: CartStore) {
        super(store);
    }

    public hasItem(id: ID): boolean {
        return this.hasEntity(id) && !!this.getEntity(id);
    }

    public getItem(id: ID): CartItem {
        return this.getEntity(id);
    }
}
