import { Injectable } from '@angular/core';
import { ID } from '@datorama/akita';
import { BaseProduct } from '../../products/state/product.model';

import { CartQuery } from './cart.query';
import { CartStore } from './cart.store';

@Injectable({ providedIn: 'root' })
export class CartService {
    constructor(private cartStore: CartStore, private cartQuery: CartQuery) {}

    public add(product: BaseProduct, quantity: number): void {
        if (quantity === 0) {
            return;
        }

        if (this.cartQuery.hasItem(product.id)) {
            quantity = this.cartQuery.getItem(product.id).quantity + quantity;
        }

        this.upsert(product, quantity);
    }

    public remove(productId: ID): void {
        if (!this.cartQuery.hasItem(productId)) {
            return;
        }

        this.cartStore.remove(productId);
    }

    public update(productId: ID, quantity: number): void {
        if (!this.cartQuery.hasItem(productId)) {
            return;
        }

        const cartItem = this.cartQuery.getItem(productId);

        if (quantity <= 0) {
            this.cartStore.remove(productId);
            return;
        }

        this.upsert(cartItem.product, quantity);
    }

    private upsert(product: BaseProduct, quantity: number): void {
        this.cartStore.upsert(product.id, {
            product: product,
            total: product.price * quantity,
            quantity,
        });
    }
}
