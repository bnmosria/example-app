import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ID } from '@datorama/akita';
import { Observable } from 'rxjs';
import { AuthQuery } from '../../auth/state/auth.query';

import { FormControl } from '@angular/forms';
import { CartItem } from '../state/cart.model';
import { CartQuery } from '../state/cart.query';
import { CartService } from '../state/cart.service';

@Component({
    templateUrl: './cart-page.component.html',
})
export class CartPageComponent {
    public items$: Observable<CartItem[]> = this.cartQuery.selectAll();
    public count$: Observable<number> = this.cartQuery.selectCount();
    public total$: Observable<number> = this.cartQuery.selectTotal$;

    public quantity: FormControl = new FormControl(1);

    constructor(
        private cartQuery: CartQuery,
        private authQuery: AuthQuery,
        private router: Router,
        private cartService: CartService
    ) {}

    public remove(productId: ID): void {
        this.cartService.remove(productId);
    }

    public update(productId: ID): void {
        this.cartService.update(productId, this.quantity.value);
    }

    public checkout(): void {
        if (this.authQuery.isLoggedIn()) {
            // checkout
        } else {
            this.router.navigateByUrl('login');
        }
    }
}
