import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { CartSummaryComponent } from '../cart/cart-summary/cart-summary.component';
import { ModelModule } from '../model/model.module';
import { ProductsModule } from '../products/products.module';
import { CartDetailComponent } from './cartDetail.component';
import { CheckoutComponent } from './checkout.component';
import { CounterDirective } from './counter.directive';
import { StoreComponent } from './store.component';
import {ProductsService} from '../products/state/products.service';

@NgModule({
    imports: [
        ModelModule,
        BrowserModule,
        FormsModule,
        RouterModule,
        ProductsModule,
    ],
    declarations: [
        StoreComponent,
        CounterDirective,
        CartDetailComponent,
        CartSummaryComponent,
        CheckoutComponent,
    ],
    exports: [StoreComponent, CartDetailComponent, CheckoutComponent],
    providers: [ProductsService],
})
export class StoreModule {}
