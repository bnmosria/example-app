import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Cart } from '../model/cart.model';
import { ProductRepository } from '../model/product.repository';
import { Category } from '../products/state/category.model';
import { BaseProduct, Product } from '../products/state/product.model';
import { ProductsQuery } from '../products/state/products.query';
import { ProductsService } from '../products/state/products.service';

@Component({
    selector: 'store',
    templateUrl: 'store.component.html',
})
export class StoreComponent implements OnInit, OnDestroy {
    public selectedCategory: Category = null;
    public productsPerPage: number = 4;
    public selectedPage: number = 1;

    public isLoading$: Observable<boolean>;
    public products$: Observable<BaseProduct[]>;
    public categories$: Observable<Category[]>;

    constructor(
        private repository: ProductRepository,
        private productsService: ProductsService,
        private productsQuery: ProductsQuery,
        private cart: Cart,
        private router: Router
    ) {}

    public ngOnInit(): void {
        this.isLoading$ = this.productsQuery.selectLoading();
        this.products$ = this.productsQuery.selectVisibleProducts();
        this.categories$ = this.productsQuery.selectCategories$;
        this.startDataFlow();
    }

    private startDataFlow(): void {
        this.productsService.getData().subscribe();
    }

    public ngOnDestroy(): void {}

    public changeCategory(category: Category): void {
        this.productsService.updateSelectedCategory(category);
        this.selectedCategory = category;
    }

    /*
    changePage(newPage: number) {
        this.selectedPage = newPage;
    }

    changePageSize(newSize: number) {
        this.productsPerPage = Number(newSize);
        this.changePage(1);
    }

    get pageCount(): number {
        return 5;
        return Math.ceil(this.repository
            .getProducts(this.selectedCategory).length / this.productsPerPage);
    }

    addProductToCart(product: Product) {
        this.cart.addLine(product);
        this.router.navigateByUrl('/cart');
    }*/
}
