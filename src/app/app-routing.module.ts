import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StoreFirstGuard } from './store-first.guard';
import { CheckoutComponent } from './store/checkout.component';
import { StoreComponent } from './store/store.component';

const routes: Routes = [
    {
        path: 'store',
        component: StoreComponent,
        canActivate: [StoreFirstGuard],
    },
    {
        path: 'checkout',
        component: CheckoutComponent,
        canActivate: [StoreFirstGuard],
    },
    {
        path: 'cart',
        loadChildren: './cart/cart.module#CartModule',
        canActivate: [StoreFirstGuard],
    },
    {
        path: 'admin',
        loadChildren: './admin/admin.module#AdminModule',
        canActivate: [StoreFirstGuard],
    },
    { path: '**', redirectTo: '/store' },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
