import { Injectable } from '@angular/core';
import {
    ActivatedRouteSnapshot,
    Router,
    RouterStateSnapshot,
} from '@angular/router';
import { Message } from './messages/message.model';
import { MessageService } from './messages/message.service';

@Injectable()
export class TermsGuard {
    constructor(private messages: MessageService, private router: Router) {}

    public canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Promise<boolean> | boolean {
        if (route.params['mode'] == 'create') {
            return new Promise<boolean>((resolve, reject) => {
                const responses: [string, (string) => void][] = [
                    ['Yes', () => resolve(true)],
                    ['No', () => resolve(false)],
                ];
                this.messages.reportMessage(
                    new Message(
                        'Do you accept the terms & conditions?',
                        false,
                        responses
                    )
                );
            });
        } else {
            return true;
        }
    }

    public canActivateChild(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Promise<boolean> | boolean {
        if (
            route.url.length > 0 &&
            route.url[route.url.length - 1].path === 'categories'
        ) {
            return new Promise<boolean>((resolve, reject) => {
                const responses: [string, (string) => void][] = [
                    ['Yes', () => resolve(true)],
                    ['No ', () => resolve(false)],
                ];

                this.messages.reportMessage(
                    new Message(
                        'Do you want to see the categories component?',
                        false,
                        responses
                    )
                );
            });
        } else {
            return true;
        }
    }
}
