const PROXY_CONFIG = [
    {
        context: [
            "/api/products",
            "/api/categories",
            "/api/product",
            "/api/orders",
            "/api/cart",
        ],
        target: "http://localhost:3000",
        secure: false
    }
];

module.exports = PROXY_CONFIG;
